import React from 'react';
import {Provider as StoreProvider} from 'react-redux';
import {Switch, Route, BrowserRouter} from 'react-router-dom';
import './App.css';
import store from './store';

import Header from "./containers/Header";
import CharactersPage from './containers/CharactersPage'
import EpisodesPage from "./containers/EpisodesPage";
import LocationPage from "./containers/LocationPage";
import MyWatchList from "./containers/MyWatchList";


const App = () => {

    return (
        <BrowserRouter>
            <Header/>
            <Switch>
                <Route exact path="/characters" component={CharactersPage}/>
                <Route exact path="/episodes" component={EpisodesPage}/>
                <Route exact path="/location" component={LocationPage}/>
                <Route exact path="/my_watch_list" component={MyWatchList}/>
            </Switch>
        </BrowserRouter>
    );
};

const AppProviders = () => (
    <StoreProvider store={store}>
        <App/>
    </StoreProvider>
);

export default AppProviders;
