import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';

const superagent = superagentPromise(_superagent, global.Promise);
const ROOT = process.env.REACT_APP_URL;
const API_ROOT = `${ROOT}/api`;
let token = '';

const handleErrors = (err) => {
    if (err && err.response && err.response.status === 401) {
    }
    return err;
};

const responseBody = (res) => {
    /* eslint-disable-next-line no-proto */
    if (res.headers['x-total-count']) res.body.__proto__.count = res.headers['x-total-count'];
    return res.body;
};

const tokenPlugin = (req) => {
    if (token) {
        req.set('Authorization', `Bearer ${token}`);
    }
};

const requests = {
    del: (url, root = API_ROOT) => superagent
        .del(`${root}${url}`)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
    get: (url, root = API_ROOT, addCount) => superagent
        .get(`${root}${url}`)
        .use(tokenPlugin)
        .set('Access-Control-Expose-Headers', 'X-Total-Count,X-Total-count')
        .end(handleErrors)
        .then((res) => responseBody(res, addCount)),
    put: (url, body) => superagent
        .put(`${API_ROOT}${url}`, body)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
    post: (url, body, root = API_ROOT) => superagent
        .post(`${root}${url}`, body)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
    patch: (url, body, root = API_ROOT) => superagent
        .patch(`${root}${url}`, body)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
    delete: (url, root = API_ROOT) => superagent
        .del(`${root}${url}`)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
};

const Character = {
    all: (id = 1) => requests.get(`/character/?page=${id}`),
    filtered: (id = 1, status = '', species = '', gender = '') => requests.get(`/character/?page=${id}&status=${status}&species=${species}&gender=${gender}`)
};

const Episode = {
    all: (id = 1) => requests.get(`/episode/?page=${id}`),
    filtered: (id = 1, name = '', episode = '') => requests.get(`/episode/?page=${id}&name=${name}&episode=${episode}`),

    byIds: (ids = []) => requests.get(`/episode/${ids.values()}`),
}

const Location = {
    allOrFiltered: (id = 1, name = '', type = '', dimension = '') => requests.get(`/location?page=${id}&name=${name}&type=${type}&dimension=${dimension}`)
}


// eslint-disable-next-line import/no-anonymous-default-export
export default {
    Character,
    Episode,
    Location,
};
