import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import EnhancedTableHead from "./components/EnhancedTableHead";
import EnhancedTableToolbar from "./components/EnhancedTableToolbar";


const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',

    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 550,
    },

    row: {
        "&.Mui-selected": {
            backgroundColor: "#b4aab9",
            "& > .MuiTableCell-root": {
                color: "black"
            }
        }
    }
}));


export const EnhancedTable = ({headCells, rows, title, withCheckbox, saveAll}) => {
    const classes = useStyles();
    const [selected, setSelected] = useState([]);
    const rowPerPage = 20;

    useEffect(() => {
        setSelected([]);
    }, [rows]);

    const getIndex = (arr, obj) => {
        let index = -1;
        for (let i = 0; i < arr?.length; i++) {
            if (arr[i].first === obj.first) {
                index = i;
                break;
            }
        }
        return index;
    }

    const handleClick = (row, index) => {
        let arr = [...selected]
        arr[index] = checkSelected(row.first)
        arr[index] = !arr[index];
        setSelected(arr)
        if (arr[index]) {
            let arrOfObjects = JSON.parse(localStorage.getItem('selected'))

            if (getIndex(arrOfObjects, row) === -1) {
                if (!arrOfObjects) {
                    arrOfObjects = []
                }
                arrOfObjects.push(row);
                localStorage.setItem('selected', JSON.stringify(arrOfObjects));
            }
        } else {
            let arrOfObjects = JSON.parse(localStorage.getItem('selected'))

            const index = getIndex(arrOfObjects, row)
            if (index > -1) {
                arrOfObjects.splice(index, 1);
            }

            localStorage.setItem('selected', JSON.stringify(arrOfObjects));
        }
    };

    const checkSelected = (id) => {
        let existInStorage = false;
        let arr = JSON.parse(localStorage.getItem('selected'));
        for (let i = 0; i < arr?.length; i++) {
            if (arr[i].first === id) {
                existInStorage = true;
                break;
            }
        }
        return existInStorage;
    }

    const emptyRows = rowPerPage - Math.min(rowPerPage, rows.length);

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <EnhancedTableToolbar title={title}/>
                <TableContainer>
                    <Table
                        className={classes.table}
                        aria-labelledby="tableTitle"
                        size={'medium'}
                        aria-label="enhanced table"
                    >
                        <EnhancedTableHead
                            headCells={headCells}
                        />
                        <TableBody>
                            {rows
                                .map((row, index) => {
                                    const labelId = `enhanced-table-cell${index}`;
                                    return (
                                        <TableRow
                                            onClick={withCheckbox ? () => handleClick(row, index) : null}
                                            tabIndex={-1}
                                            key={index}
                                            className={classes.row}
                                            selected={withCheckbox ? checkSelected(row.first) || selected[index] : null}
                                            hover
                                        >
                                            <TableCell component="th" id={labelId} scope="row"
                                                       key={row.first + labelId + 'first'}>
                                                {row.first}
                                            </TableCell>
                                            <TableCell align="center" id={labelId}
                                                       key={row.second + labelId + 'second'}>
                                                {row.second}
                                            </TableCell>
                                            <TableCell align="center" id={labelId}
                                                       key={row.third + labelId + 'third'}>
                                                {row.third}
                                            </TableCell>
                                            <TableCell align="right" id={labelId} key={row.last + labelId + 'last'}>
                                                {row.last}
                                            </TableCell>
                                        </TableRow>
                                    );
                                })}
                            {emptyRows > 0 && (
                                <TableRow style={{height: 53 * emptyRows}}>
                                    <TableCell colSpan={6}/>
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        </div>
    );
}

EnhancedTable.propTypes = {
    headCells: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,

    })),
    rows: PropTypes.arrayOf(PropTypes.shape({
        first: PropTypes.string.isRequired,
        second: PropTypes.string.isRequired,
        third: PropTypes.string.isRequired,
        last: PropTypes.string.isRequired,
    })),
    title: PropTypes.string.isRequired,
    withCheckbox: PropTypes.bool,
    saveAll: PropTypes.bool,
};

EnhancedTable.defaultProps = {
    title: '',
    withCheckbox: false,
    saveAll: false,
};
