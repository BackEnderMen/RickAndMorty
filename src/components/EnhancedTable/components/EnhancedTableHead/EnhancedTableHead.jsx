import React from "react";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import PropTypes from "prop-types";
import {Typography} from "@material-ui/core";
import Box from "@material-ui/core/Box";

export const EnhancedTableHead = (props) => {
    const {headCells} = props;
    const length = headCells.length

    return (
        <TableHead>
            <TableRow>
                {headCells.map((headCell, i) => (
                    <TableCell
                        size={'small'}
                        key={headCell.id}
                        align={i === 0 ? 'left' : i + 1 === length ? 'right' : 'center'}
                        padding={'default'}
                    >
                        <Typography>
                            <Box fontWeight="fontWeightBold" m={1}>
                                {headCell.label}
                            </Box>
                        </Typography>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
};

EnhancedTableHead.propTypes = {
    headCells: PropTypes.array.isRequired,
};