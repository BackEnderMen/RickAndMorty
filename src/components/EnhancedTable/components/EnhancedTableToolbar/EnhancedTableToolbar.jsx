import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";

const useToolbarStyles = makeStyles((theme) => ({
    title: {
        flex: '1 1 100%',
    },
}));

export const EnhancedTableToolbar = ({title}) => {
    const classes = useToolbarStyles();
    return (
        <Toolbar>
            <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                {title}
            </Typography>
        </Toolbar>
    );
};

EnhancedTableToolbar.propTypes = {
    title: PropTypes.string.isRequired
};
