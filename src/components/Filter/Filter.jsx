import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import PropTypes from 'prop-types';

export const Filter = ({values, label, onChange}) => {
    const [value, setValue] = React.useState('');

    return (
        <Autocomplete
            value={value}
            onChange={(event, newValue) => {
                setValue(newValue);
                onChange(newValue)
            }}
            id={"controllable-states-demo" + label}
            options={values}
            style={{width: 200, margin: '1em 0'}}
            renderInput={(params) => <TextField {...params} label={label} variant="outlined"/>}
        />
    );
}

Filter.propTypes = {
    onChange: PropTypes.func,
    values: PropTypes.array.isRequired,
    label: PropTypes.string.isRequired
};

Filter.defaultProps = {
    onChange: () => {
    },
};
