import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
    root: {
        maxWidth: 320,
        margin: 15,
    },
    media: {
        height: 140,
        width: 320,
    },
});

export const MediaCard = ({onClick, species, status, gender, img, name}) => {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardActionArea>
                <CardMedia
                    className={classes.media}
                    image={img}
                    title="Contemplative Reptile"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {name}
                    </Typography>
                    <Typography gutterBottom variant="subtitle2" component="h3">
                        Gender: {gender}
                    </Typography>
                    <Typography gutterBottom variant="subtitle2" component="h3">
                        Species: {species}
                    </Typography>
                    <Typography gutterBottom variant="subtitle2" component="h3">
                        Status: {status}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Button size="medium" color="primary" onClick={onClick}>
                    See more
                </Button>
            </CardActions>
        </Card>
    );
}

MediaCard.propTypes = {
    onClick: PropTypes.func,
    species: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    gender: PropTypes.string.isRequired,
    img: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
};

MediaCard.defaultProps = {
    onClick: () => {
    },
};