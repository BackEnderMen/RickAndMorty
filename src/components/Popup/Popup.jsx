import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import PropTypes from "prop-types";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
    root: {},
    media: {
        height: 250,
        width: 650,
    },
});

export const Popup = ({open, handleClose, title, img, species, status, gender, origin, location, episodeAmount}) => {
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const classes = useStyles();

    return (
        <div>
            <Dialog
                fullScreen={fullScreen}
                open={open}
                onClose={handleClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title">{title}</DialogTitle>
                <CardMedia
                    className={classes.media}
                    image={img}
                    title="Contemplative Reptile"
                />
                <DialogContent style={{display: 'flex', justifyContent: 'space-between'}}>
                    <div>
                        <Typography gutterBottom variant="subtitle2" component="h2">
                            Gender: {gender}
                        </Typography>
                        <Typography gutterBottom variant="subtitle2" component="h2">
                            Species: {species}
                        </Typography>
                        <Typography gutterBottom variant="subtitle2" component="h2">
                            Status: {status}
                        </Typography>
                    </div>
                    <div>
                        <Typography gutterBottom variant="subtitle2" component="h2">
                            Origin: {origin}
                        </Typography>
                        <Typography gutterBottom variant="subtitle2" component="h2">
                            Location: {location}
                        </Typography>
                    </div>
                </DialogContent>
                <DialogContent>
                    <DialogContentText>
                        Episodes`s amount where person was involved: {episodeAmount}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary" size="large" autoFocus>
                        Okay
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

Popup.propTypes = {
    handleClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    img: PropTypes.string.isRequired,

    species: PropTypes.string,
    status: PropTypes.string,
    gender: PropTypes.string,
    origin: PropTypes.string,
    location: PropTypes.string,
    episodeAmount: PropTypes.number,
};

Popup.defaultProps = {
    title: 'title',
    img: 'img',
    species: 'Unknown',
    status: 'Unknown',
    gender: 'Unknown',
    origin: 'Unknown',
    location: 'Unknown',
    episodeAmount: 0,
};