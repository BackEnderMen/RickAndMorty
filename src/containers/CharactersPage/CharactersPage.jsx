import React, {useEffect, useState} from 'react'
import useCharacter from "../../hooks/useCharacter";
import {MediaCard} from "../../components/MediaCard/MediaCard";
import styles from './CharacterPage.module.scss'
import Pagination from '@material-ui/lab/Pagination';
import {Filter} from "../../components/Filter/Filter";
import {Typography} from "@material-ui/core";
import {Popup} from "../../components/Popup/Popup";


export const CharactersPage = () => {

    const [page, setPage] = useState(1);
    const [openPopup, setOpenPopup] = useState(false);
    const [activeCharacterId, setActiveCharacterId] = useState(0);

    const [species, setSpecies] = useState('');
    const [status, setStatus] = useState('');
    const [gender, setGender] = useState('');

    const {getAllCharacter, getFilteredCharacter, result, info} = useCharacter();


    const handleChangePage = (event, value) => {
        setPage(value)
        fetch(value, species, status, gender)
    };

    const fetch = (id, species, status, gender) => {
        if (species !== '' || status !== '' || gender !== '') {
            getFilteredCharacter(id, status, species, gender).then()
        } else {
            getAllCharacter(id).then()
        }
    }

    const onSpeciesFilter = (value) => {
        if (value === null) {
            setSpecies('')
            setPage(1)
            fetch(1, '', status, gender)
        } else {
            setSpecies(value)
            setPage(1)
            fetch(1, value, status, gender)
        }
    }
    const onStatusFilter = (value) => {
        if (value === null) {
            setStatus('')
            setPage(1)
            fetch(1, species, '', gender)
        } else {
            setStatus(value)
            setPage(1)
            fetch(1, species, value, gender)
        }
    }
    const onGenderFilter = (value) => {
        if (value === null) {
            setGender('')
            setPage(1)
            fetch(1, species, status, '')
        } else {
            setGender(value)
            setPage(1)
            fetch(1, species, status, value)
        }
    }

    const onOpenPopup = (id) => {
        setActiveCharacterId(id)
        setOpenPopup(true)
    }

    useEffect(() => {
        if (typeof info === "undefined" || info === null) {
            getAllCharacter().then()
        }
    })

    return (
        <>
            <div className={styles.container}>
                CharactersPage
                <Pagination count={info?.pages} page={page} variant='outlined' shape='rounded'
                            style={{margin: '1em 0'}}
                            onChange={handleChangePage}/>
                <div className={styles.content}>
                    {
                        result?.map((value, id) => {
                            return <MediaCard status={value.status} gender={value.gender} img={value.image}
                                              name={value.name} species={value.species}
                                              onClick={() => {
                                                  onOpenPopup(id)
                                              }} key={"MediaCard" + id}/>
                        })
                    }
                </div>
                <Pagination count={info?.pages} page={page} variant='outlined' shape='rounded'
                            style={{margin: '1em 0'}}
                            onChange={handleChangePage}/>
            </div>
            <div className={styles.filter}>
                <Typography variant={"h4"} children={'Filter by'}/>
                <Filter label={'Species'}
                        values={["Human", "Alien", "Poopybutthole", "Humanoid", "Mythological Creature", "Animal", "Robot", "Disease", "Unknown", "Cronenberg", "Planet"]}
                        onChange={onSpeciesFilter}/>
                <Filter label={'Status'} values={["Alive", "Dead", "Unknown"]} onChange={onStatusFilter}/>
                <Filter label={'Gender'} values={["Female", "Male", "Genderless", "Unknown"]}
                        onChange={onGenderFilter}/>
            </div>

            <Popup title={result[activeCharacterId]?.name}
                   img={result[activeCharacterId]?.image}
                   species={result[activeCharacterId]?.species}
                   status={result[activeCharacterId]?.status}
                   gender={result[activeCharacterId]?.gender}
                   origin={result[activeCharacterId]?.origin.name}
                   location={result[activeCharacterId]?.location.name}
                   episodeAmount={result[activeCharacterId]?.episode.length}
                   open={openPopup} handleClose={() => {
                setOpenPopup(false)
            }}/>
        </>
    )
}