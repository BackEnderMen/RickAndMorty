import React, {useEffect, useState} from 'react'
import useEpisode from "../../hooks/useEpisode";
import EnhancedTable from "../../components/EnhancedTable";
import styles from './EpisodesPage.module.scss'
import {Typography} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import {useDebounce} from "../../hooks/useDebouns";
import Pagination from "@material-ui/lab/Pagination";

const DELAY = 1000; // delays in ms

export const EpisodesPage = () => {
    const [rows, setRows] = useState([]);
    const [name, setName] = useState('')
    const [page, setPage] = useState(1);

    const {getFilteredEpisode, result, info} = useEpisode();
    const debouncedText = useDebounce(name, DELAY);


    useEffect(() => {
        loadRows();
    }, [result])

    useEffect(() => {
        getFilteredEpisode(1, name).then()
    }, [debouncedText])

    const loadRows = () => {
        let arr = []
        result.forEach((item) => {
            arr.push({
                first: item.id.toString(),
                second: item.name,
                third: item.air_date,
                last: item.characters.length.toString()
            })
        })
        setRows(arr)
    }

    const handleChangePage = (event, value) => {
        setPage(value);
        getFilteredEpisode(value, name).then();
    };

    return (
        <>
            <div className={styles.container}>
                <Pagination count={info?.pages} page={page} variant='outlined' shape='rounded'
                            style={{margin: '1em 0'}}
                            onChange={handleChangePage}/>
                <div className={styles.tableContainer}>
                    <EnhancedTable rows={rows} headCells={headCells} title={'EpisodePage'}/>
                </div>
                <Pagination count={info?.pages} page={page} variant='outlined' shape='rounded'
                            style={{margin: '1em 0'}}
                            onChange={handleChangePage}/>
            </div>
            <div className={styles.filter}>
                <Typography variant={"h4"} children={'Filter by'}/>
                <div className={styles.textField}>
                    <TextField id="outlined-basic" label="Name" variant="outlined" style={{width: '10em'}} value={name}
                               onChange={(e) => {
                                   setName(e.target.value)
                               }}/>
                </div>
            </div>
        </>
    )
}


const headCells = [
    {id: 'id', label: 'Id'},
    {id: 'name', label: 'Name'},
    {id: 'air_date', label: 'Date'},
    {id: 'episodes', label: 'Amount of Characters'},
];