import React from 'react';
import {NavLink} from 'react-router-dom';
import styles from './Header.module.scss'

const handleClickBurger = () => {
    const nav = document.getElementsByClassName(styles.nav)[0];

    if (nav.classList.contains(styles.hiddenNav)) {
        nav.classList.add(styles.showNav);
        nav.classList.remove(styles.hiddenNav);
    } else {
        nav.classList.remove(styles.showNav);
        nav.classList.add(styles.hiddenNav);
    }
};

export const Header = () => {

    return (
        <header className={styles.header}>
            <div className={`${styles.nav} ${styles.hiddenNav}`}>
                <NavLink exact to="/characters" activeClassName={styles.active} onClick={handleClickBurger}>
                    Characters
                </NavLink>
                <NavLink exact to="/episodes" activeClassName={styles.active} onClick={handleClickBurger}>
                    Episodes
                </NavLink>
                <NavLink exact to="/location" activeClassName={styles.active} onClick={handleClickBurger}>
                    Locations
                </NavLink>
                <NavLink exact to="/my_watch_list" activeClassName={styles.active} onClick={handleClickBurger}>
                    MyWatchlist
                </NavLink>
            </div>
        </header>
    );
};
