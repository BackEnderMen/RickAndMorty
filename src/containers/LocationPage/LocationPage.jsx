import React, {useEffect, useState} from 'react'
import Pagination from "@material-ui/lab/Pagination";
import EnhancedTable from "../../components/EnhancedTable";
import {Typography} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import styles from './LocationPage.module.scss'
import useLocation from "../../hooks/useLocation";
import {useDebounce} from "../../hooks/useDebouns";

const DELAY = 1000; // delays in ms

export const LocationPage = () => {
    const [page, setPage] = useState(1);
    const [rows, setRows] = useState([]);
    const [name, setName] = useState('')
    const [type, setType] = useState('')
    const [dimension, setDimension] = useState('')

    const {getAllOrFiltered, result, info} = useLocation()
    const debouncedName = useDebounce(name, DELAY);
    const debouncedType = useDebounce(type, DELAY);
    const debouncedDimension = useDebounce(dimension, DELAY);

    useEffect(() => {
        loadRows();
    }, [result])

    useEffect(() => {
        getAllOrFiltered(1, name, type, dimension).then()
    }, [debouncedName, debouncedType, debouncedDimension])

    const loadRows = () => {
        let arr = []
        result.forEach((item) => {
            arr.push({
                first: item.id.toString(),
                second: item.name,
                third: item.type,
                last: item.dimension
            })
        })
        setRows(arr)
    }

    const handleChangePage = (event, value) => {
        setPage(value);
        getAllOrFiltered(value, name, type, dimension).then();
    };
    return (
        <>
            <div className={styles.container}>
                <Pagination count={info?.pages} page={page} variant='outlined' shape='rounded'
                            style={{margin: '1em 0'}} onChange={handleChangePage}
                />
                <div className={styles.tableContainer}>
                    <EnhancedTable rows={rows} headCells={headCells} title={'LocationPage'}/>
                </div>
                <Pagination count={info?.pages} page={page} variant='outlined' shape='rounded'
                            style={{margin: '1em 0'}} onChange={handleChangePage}
                />
            </div>
            <div className={styles.filter}>
                <Typography variant={"h4"} children={'Filter by'}/>
                <div className={styles.textField}>
                    <TextField id="outlined-basic" label="Name" variant="outlined" style={{width: '10em'}} value={name}
                               onChange={(e) => {
                                   setName(e.target.value)
                               }}/>
                </div>
                <div className={styles.textField}>
                    <TextField id="outlined-basic" label="Type" variant="outlined" style={{width: '10em'}} value={type}
                               onChange={(e) => {
                                   setType(e.target.value)
                               }}/>
                </div>
                <div className={styles.textField}>
                    <TextField id="outlined-basic" label="Dimension" variant="outlined" style={{width: '10em'}}
                               value={dimension}
                               onChange={(e) => {
                                   setDimension(e.target.value)
                               }}/>
                </div>
            </div>
        </>
    )
}

const headCells = [
    {id: 'id', label: 'Id'},
    {id: 'name', label: 'Name'},
    {id: 'type', label: 'Type'},
    {id: 'dimension', label: 'Dimension'},
];