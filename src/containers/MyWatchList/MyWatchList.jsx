import React, {useEffect, useState} from 'react'
import styles from './MyWatchList.module.scss'
import Pagination from "@material-ui/lab/Pagination";
import EnhancedTable from "../../components/EnhancedTable";
import TextField from "@material-ui/core/TextField";
import useEpisode from "../../hooks/useEpisode";
import {useDebounce} from "../../hooks/useDebouns";

const DELAY = 1000; // delays in ms

export const MyWatchList = () => {
    const [rows, setRows] = useState([]);
    const [submitted, setSubmitted] = useState(false);
    const [name, setName] = useState('');
    const [page, setPage] = useState(1);


    const {getFilteredEpisode, result, info} = useEpisode();
    const debouncedText = useDebounce(name, DELAY);


    useEffect(() => {
        loadRows();
    }, [result])

    useEffect(() => {
        getFilteredEpisode(1, name).then()
    }, [debouncedText])

    const loadRows = () => {
        let arr = []
        result.forEach((item) => {
            arr.push({
                first: item.id.toString(),
                second: item.name,
                third: item.air_date,
                last: item.characters.length.toString()
            })
        })
        setRows(arr)
    }

    const handleChangePage = (event, value) => {
        setPage(value);
        getFilteredEpisode(value, name).then();
    };
    return (
        <>
            <div className={styles.container}>
                <div className={styles.tableContainer}>
                    <div className={styles.control}>
                        <TextField id="outlined-basic" label="Search" variant="outlined"
                                   style={{width: '25vw', margin: '1em 0'}} value={name}
                                   onChange={(e) => {
                                       setName(e.target.value)
                                   }}/>

                    </div>
                    <EnhancedTable rows={rows} headCells={headCells} title={'Choose what you want to watch later.'}
                                   withCheckbox={true}/>
                </div>
                <Pagination count={info?.pages} page={page} variant='outlined' shape='rounded'
                            style={{margin: '1em 0'}}
                            onChange={handleChangePage}/>
            </div>
        </>
    )
}

const headCells = [
    {id: 'id', label: 'Id'},
    {id: 'name', label: 'Name'},
    {id: 'air_date', label: 'Date'},
    {id: 'episodes', label: 'Amount of Characters'},
];