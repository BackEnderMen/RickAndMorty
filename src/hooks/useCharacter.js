import {useSelector} from 'react-redux';
import useAsyncDispatch from './useAsyncDispatch';
import {getAllCharacter, getFilteredCharacter} from "../store/actions";


export default function useCharacter() {
    const character = useSelector((state) => state.character);
    const dispatch = useAsyncDispatch();

    return {
        ...character,
        getAllCharacter: (id) => dispatch(getAllCharacter(id)),
        getFilteredCharacter: (id, status, species, gender) => dispatch(getFilteredCharacter(id, status, species, gender))
    };
}
