import {useSelector} from 'react-redux';
import useAsyncDispatch from './useAsyncDispatch';
import {getAllEpisode, getFilteredEpisode} from "../store/actions";


export default function useEpisode() {
    const episode = useSelector((state) => state.episode);
    const dispatch = useAsyncDispatch();

    return {
        ...episode,
        getAllEpisode: (id) => dispatch(getAllEpisode(id)),
        getFilteredEpisode: (id, name, episode) => dispatch(getFilteredEpisode(id, name, episode))
    };
}