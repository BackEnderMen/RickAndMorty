import {useSelector} from 'react-redux';
import useAsyncDispatch from './useAsyncDispatch';
import {getAllOrFilteredLocation} from "../store/actions";


export default function useLocation() {
    const location = useSelector((state) => state.location);
    const dispatch = useAsyncDispatch();

    return {
        ...location,
        getAllOrFiltered: (id, name, type, dimension) => dispatch(getAllOrFilteredLocation(id, name, type, dimension)),
    };
}