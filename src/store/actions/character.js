import {createAction} from 'redux-actions';
import agent from '../../agent';

export const getAllCharacter = createAction('ALL/CHARACTER', async (id) => {
    return await agent.Character.all(id);
});

export const getFilteredCharacter = createAction('FILTERED/CHARACTER', async (id, status, species, gender) => {
    return await agent.Character.filtered(id, status, species, gender);
});
