import {createAction} from 'redux-actions';
import agent from '../../agent';

export const getAllEpisode = createAction('ALL/EPISODE', async (id) => {
    return await agent.Episode.all(id);
});

export const getFilteredEpisode = createAction('FILTERED/EPISODE', async (id, name, episode) => {
    return await agent.Episode.filtered(id, name, episode);
});