import {createAction} from 'redux-actions';
import agent from '../../agent';

export const getAllOrFilteredLocation = createAction('ALL/FILTERED/LOCATION', async (id, name, type, dimension) => {
    return await agent.Location.allOrFiltered(id, name, type, dimension);
});