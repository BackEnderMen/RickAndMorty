import {ActionType} from "redux-promise-middleware";
import {handleActions} from "redux-actions";
import {getAllEpisode, getFilteredEpisode} from "../actions";

const reducer = handleActions(
    {
        [getAllEpisode.toString()]: {
            [ActionType.Fulfilled]: (state, action) => ({
                ...state,
                result: action.payload.results,
                info: action.payload.info,
            }),
            [ActionType.Rejected]: (state, action) => ({
                ...state,
                ...action.payload,
                result: [],
                info: null,
            }),
        },
        [getFilteredEpisode.toString()]: {
            [ActionType.Fulfilled]: (state, action) => ({
                ...state,
                result: action.payload.results,
                info: action.payload.info,
            }),
            [ActionType.Rejected]: (state, action) => ({
                ...state,
                ...action.payload,
                result: [],
                info: null,
            }),
        },
    },
    {
        info: null,
        result: []
    }
);

export default reducer;