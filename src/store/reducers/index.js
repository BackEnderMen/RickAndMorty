import {combineReducers} from 'redux';
import character from "./character";
import episode from './episode'
import location from './location'

const reducers = combineReducers({
    character,
    episode,
    location
});

export default reducers;
