import {ActionType} from "redux-promise-middleware";
import {handleActions} from "redux-actions";
import {getAllOrFilteredLocation} from "../actions";

const reducer = handleActions(
    {
        [getAllOrFilteredLocation.toString()]: {
            [ActionType.Fulfilled]: (state, action) => ({
                ...state,
                result: action.payload.results,
                info: action.payload.info,
            }),
            [ActionType.Rejected]: (state, action) => ({
                ...state,
                ...action.payload,
                result: [],
                info: null,
            }),
        },

    },
    {
        info: null,
        result: []
    }
);

export default reducer;